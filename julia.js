import {vec2, dot, add, sub, mult, div, clamp, length, uv0, uv1} from './vec2.js';
const {sin, cos, atan2, log, exp, floor, ceil, min, max, round, abs, sqrt, PI} = Math;

let thresh = 4;
let stop = 80;

function cmul(a, b){
    return vec2(a.x*b.x - a.y*b.y, a.x*b.y + a.y*b.x);
}

function cdiv(v1, v2){
    let a = (v1.x*v2.x+v1.y*v2.y)/(v2.x*v2.x+v2.y*v2.y);
    let b = (v1.y*v2.x-v1.x*v2.y)/(v2.x*v2.x+v2.y*v2.y);
    return vec2(a,b);
}

function cexp(z){
	let e = exp(z.x);
    return vec2(e*cos(z.y),e*sin(z.y));
}

function clog(z){
    return vec2(log(length(z)),atan2(z.y,z.x));
}

function cpow(z, /*float*/ c){
    return cexp(mult(c,clog(z)));
}


function clerp(a, b, /*float*/ m){
    m = clamp(m,0.,1.);
    return add(cmul(vec2(1.-m),a),cmul(vec2(m),b));
}


function rot(v, /*float*/ t){
    let a = atan2(v.x,v.y)+t;
    return mult(length(v),vec2(cos(a),sin(a)));
}

function julia(coord, res, t, mouse){
	let z = uv1(coord, res);

   // z = mult(.2, clog(add(1,z)));
   // z = clerp( mult(.1, clog(add(1,z))), cdiv(vec2(.5),z), .08);
    // z = cexp(cmul(z,vec2(1)));
    // z=rot(z, t*.2);
    let c = mouse.x===0&&mouse.y===0? /*vec2(-.77,-.22)*/ vec2(.8*cos(t*.2)-.3,-.66): mouse; 
    let i = 0;
    for(i; i < stop; i++){
        let m = vec2(z.x*z.x - z.y*z.y, z.x*z.y + z.y*z.x)
        z = add(cmul(z,z),c);
        if(dot(z,z) > thresh)
            break;
    }
    return log(.9+(i/50.))*2.;
}

export default julia;