function frame(pre, x, y, palette, n, shader, t, mouse){
	let str = '';
	for(let iy = 0; iy < y; iy++){
		for(let ix = 0; ix < x; ix++){
			let a = shader({x:ix,y:iy*2}, {x:x,y:y*2}, t, mouse);
			str += palette[Math.max(0,Math.round(Math.min(a*n,n)))];
		}
		str += '\n';
	}
	pre.innerHTML = str;
}

function runShader(pre, css, scale, x, y, fps, palette, shader){
	let t = 0;
	let mouse = {x: 0, y: 0};
	let p = palette;
	let n = palette.length-1;
	pre.style.width = x+'ch';
	pre.style.fontSize = scale*2+'ch';
	for(let i in css){
		pre.style[i] = css[i];
	}
	pre.onmousemove = (e)=>{
		mouse.x = e.clientX/(4*scale*x)-1;
		mouse.y = e.clientY/(8*scale*y)-1; 
	}
	return window.setInterval(()=>{
		t += .1;
		frame(pre, x, y, p, n, shader.shader, t, mouse);
	}, fps);
}

export default runShader;