import run from './frag.js';
import {vec2,  vec3, vec4, dot, add, sub, mult, div, clamp, absv, length, step, fract, rotvz, uv0, uv1} from './vec2.js';
const {sin, cos, log, floor, ceil, min, max, round, abs, sqrt, PI} = Math;
import julia from './julia.js';

const shade = (coord, res, t, mouse)=>{
	let uv = uv1(coord, res);
	let v = sub(uv,mouse);
	let a = .04/dot(v,v);
	return a;
}

const shade1 = (coord, res, t, mouse)=>{
	let uv = uv1(coord, res);
	let a = dot(uv,uv);
	return (sin(t+a*8)*.5+.5);
}

const shade2 = (coord, res, t, mouse)=>{
	let uv = uv1(coord, res);
	let x = sin(t+uv.x*8*mouse.x)*.5;
	let a = (.04/(mouse.y+1)*.4)/abs(x-uv.y);
	return a;
}

function fold(uv, time, p, num){
	for(let i = 0; i < num; i++){	
		p.x *= (p.y+i*sin(0.1*time+p.x+(p.y*uv.x*7.)))*.9
		p.y *= (p.x+i*sin(0.1*time+p.y+(p.x*uv.y*6.)))*.9	
	}
	return sin(p.x*p.y)+.2;
}

function hm(uv, v, t){
    let a = dot(uv,uv);
    let b = (sin(t+uv.x/a/v.x))-cos(uv.y/a/v.y);
	return abs(b*1.4)+a;
}

function ball(r, t, b, c){
    let x = sin(t*c.x)*cos(b.y*t*c.y);
    let y = cos(t*c.z)*sin(t*c.w);
    return vec2(x*r, y*r);
}

const meta = (coord, res, t, mouse)=>{
	let uv = mult(1.4, uv1(coord, res));
	let r = 1, c = 0;
	let b1 = vec2(1);
    
	for(let i=0; i < 9.; i++){
        b1 = ball(.9, 7+t*.5, mult(.001,b1), vec4(cos(3.3*i), cos(4.4*i), cos(5.5*i), cos(6.6*i)));
        let v = sub(uv, mult(r, b1));
        c += r/dot(v,v);
    }
    // c *= c*0.002;
    c *= 0.05;
    c = step(1./c,.9)- step(1./c,.7)
    return c;
}

const shade3 = (coord, res, t, mouse)=>{
	let uv = uv0(coord, res);
	let a = fold(uv, t*2, {x: 2.5, y: .34}, 4);
	return a+.3;
}

const shade4 = (coord, res, t, mouse)=>{
	let uv = mult(10, uv1(coord, res));	
	let m = mouse.x===0&&mouse.y===0? vec2(.2) : mouse;
	for(let i = 0; i < 3; i++){
		uv = absv(sub(div(uv, vec2( hm(uv, vec2(i*.2+.5), t*.4)) ) , mult(-.7,absv(m))));
	}
	return 1.-uv.x;
}

const rose = (coord, res, t, mouse)=>{
	let uv = uv1(coord, res);
	let c = 0;
	for(let i = 1; i < 50; i++){
		let n  = .55*t-i*.05;
		let v = vec2(cos(n), sin(n));
		v = mult(.8*sin(5*n), v);
		let _v = sub(uv,v);
		c += (.0005/i)/dot(_v,_v);
	}
	return c;
}

const _julia = (coord, res, t, mouse)=>{
	let z = uv1(coord, res);
    let c = mouse.x===0&&mouse.y===0? /*vec2(-.77,-.22)*/ vec2(.8*cos(t*.2)-.3,-.66): mouse; 
    let i = 0;
    for(i; i < 60; i++){
        z = add(vec2(z.x*z.x - z.y*z.y, z.x*z.y + z.y*z.x),c);
        if(dot(z,z) > 4)
            break;
    }
    return log(.9+(i/50.))*2.;
}

function sn(uv, ofs, amp, t){
	let v = sub(uv, ofs)
	return sin(t+dot(v,v)*amp)*.5+.5;
}

const plasma = (coord, res, t, mouse)=>{
	let uv = uv0(coord, res);
	t *= .45
	let c = sn(uv,vec2(0.5),2.0,1.0) +
	sn(uv,vec2(0.1,0.7),20.0,t*2.) +
	sn(uv,mouse,10.0,t*5.);

	let b = sn(uv,vec2(0.3),10.0,3.0) +
	sn(uv,vec2(0.3,0.7),c*10.0,t*1.5) +
	sn(uv,mouse,c*2.0,t*2.5);

	return (c*0.2-log((1.1+b)*0.4));
}

const wave = (coord, res, t, mouse)=>{
	let p = uv1(coord, res);
	for(let n=1; n<5; n++){
		p = add(p, vec2(
		0.4*(sin(i*p.y+t*.4))+0.9,
		0.2*log(sin((p.x*p.y)+t*.6)+1.)+1.2
		));
	}
	return cos(p.y+p.x)*.5+.5;
}

const grid = (coord, res, t, mouse)=>{
	let uv = add(.1, uv0(coord, res));
	uv = rotvz(uv, t*.1);
	let l = div(.1, absv(add(-.5, fract(mult(4, uv)))));
	let f = max(l.x, l.y)*5;
	// return f*f*f;
	return step(.9, f);
}

const grid2 = (coord, res, t, mouse)=>{
	let uv = add(.1, uv0(coord, res));
	uv = vec3(uv.x, uv.y, 1);
	let m = mouse.x===0&&mouse.y===0? vec2(.3) : mouse;
	uv.z = uv.x*(m.x+.0)*6+uv.y*(m.y+.7)*6; 
	uv = rotvz(uv, t*.1);
	let z = 1./(2-uv.z*.5);
	uv = mult(z, uv);
	let l = div(.1, absv(add(-.5, fract(mult(4, uv)))));
	let f = max(l.x, l.y)*5;
	// f = f*f*f;
	f = 1 - f*f*f;
	// f = step(.9, f);
	return f/(z*z);
}

let b = document.createElement('button');
b.innerHTML = '>';
b.style.width = '120px';
b.style.padding = '4px';
b.style.margin = '5px';
document.body.appendChild(b);
let funcs = [shade, shade1, shade2, shade3, shade4, meta, rose, _julia, wave, plasma, grid2];
let i = 10;
let func = {shader:funcs[i]};
b.onclick = (e)=>{
	i = ++i%funcs.length;
	func.shader = funcs[i];
}


let css = {'backgroundColor' : 'black', 'color' : '#aee4ff'};
run(document.querySelector('#disp'), css, .7, 100, 50, 30, ' .-~=/)oeg', func); 

