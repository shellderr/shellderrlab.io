import terser from '@rollup/plugin-terser';

export default {
    input: 'main.js', 
    output: [
        {
            file: 'public/bundle.js',
            format: 'iife',
            plugins: [terser()]
        }
    ]
};