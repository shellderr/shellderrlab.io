function vec2(a, b){
	return {x:a, y:b||a};
}
function vec3(a, b, c){
	return {x:a, y:b||a, z: c||a};
}
function vec4(a, b, c, d){
	return {x:a, y:b||a, z: c||a, w: d||a};
}
function dot(a, b){
	return a.x*b.x + a.y*b.y;
}
function add(a, b){ // left can be scalar
	return a.x? {x: a.x+b.x, y: a.y+b.y} : {x: a+b.x, y: a+b.y};
}
function sub(a, b){
	return a.x? {x: a.x-b.x, y: a.y-b.y} : {x: a-b.x, y: a-b.y};
}
function mult(a, b){
	return a.x? {x: a.x*b.x, y: a.y*b.y} : {x: a*b.x, y: a*b.y};
}
function div(a, b){
	return a.x? {x: a.x/b.x, y: a.y/b.y} : {x: a/b.x, y: a/b.y};
}
function absv(v){
	return {x: Math.abs(v.x), y: Math.abs(v.y)};
}
function logv(v){
	return {x: Math.log(v.x), y: Math.log(v.y)};
}
function uv0(c, r){
	r.x += .0001;
	c.x+=.0001;
	return {x: c.x/r.x, y: c.y/r.y};
}
function uv1(c, r){
	c.x += .0001
	r.y += .0001;
	return {x: (2*c.x-r.x)/r.y, y: (2*c.y-r.y)/r.y};
}
function clamp(f, l, h){
	return Math.min(Math.max(f,l),h);
}
function length(v){
	return Math.sqrt(v.x*v.x+v.y+v.y);
}
function step(e, f){
	return f > e ? 1: 0;
}
function fract(v){
	return v.x ? {x: Math.floor(v.x)-v.x, y: Math.floor(v.y)-v.y} : Math.floor(v)-v;
}

function rotz3(t){
	return [[Math.cos(t), Math.sin(t), 0], [-Math.sin(t), Math.cos(t), 0], [0, 0, 1]];
}

function multv3(v, m){
	return [v.x*m[0][0] + v.y*m[1][0] + v.z*m[2][0],
	 		v.x*m[0][1] + v.y*m[1][1] + v.z*m[2][1],
	 		v.x*m[0][2] + v.y*m[1][2] + v.z*m[2][2]];
}

function rotvz(v, t){
	return {x: (v.x*Math.cos(t) + v.y*-Math.sin(t) + 0), y: (v.x*Math.sin(t) + v.y*Math.cos(t) + 0), z: v.z};
}

export{vec2, vec3, vec4, dot, add, sub, mult, div, clamp, absv, logv, step, fract, length, rotvz, uv0, uv1};